# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# 
# 
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files
# 
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: installation-report@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:48+0100\n"
"PO-Revision-Date: 2010-03-12 21:00+0530\n"
"Last-Translator: Sampada <sampadanakhare@gmail.com>\n"
"Language-Team: Marathi, janabhaaratii, C-DAC, Mumbai, India "
"<janabhaaratii@cdacmumbai.in>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../save-logs.templates:1001
msgid "Save debug logs"
msgstr "दोषमार्जन सत्रनोंदी संचयित करा"

#. Type: select
#. Choices
#. Possible locations for debug logs to be saved
#. :sl2:
#: ../save-logs.templates:2001
msgid "floppy"
msgstr "फ्लॉपी"

#. Type: select
#. Choices
#. Possible locations for debug logs to be saved
#. :sl2:
#: ../save-logs.templates:2001
msgid "web"
msgstr "महाजाल"

#. Type: select
#. Choices
#. Possible locations for debug logs to be saved
#. :sl2:
#: ../save-logs.templates:2001
msgid "mounted file system"
msgstr "आरोहित फाइल प्रणाली"

#. Type: select
#. Description
#. :sl2:
#: ../save-logs.templates:2002
msgid "How should the debug logs be saved or transferred?"
msgstr "दोषमार्जन सत्रनोंदी कशा पद्धतीने संचयित करायची अथवा स्थानांतरित करायची?"

#. Type: select
#. Description
#. :sl2:
#: ../save-logs.templates:2002
msgid ""
"Debugging log files for the installer can be saved to floppy, served up over "
"the web, or saved to a mounted file system."
msgstr ""
"अधिष्ठापकाच्या दोषमार्जन सत्रनोंद फायली फॉपीत संचयित करता येईल, महाजालावर पाठवता "
"येईल, किंवा आरोहित फाइल प्रणालीत साठवता येइल."

#. Type: string
#. Description
#. :sl2:
#: ../save-logs.templates:3001
msgid "Directory in which to save debug logs:"
msgstr "दोषमार्जन सत्रनोंदी साठवायची निर्देशिका:"

#. Type: string
#. Description
#. :sl2:
#: ../save-logs.templates:3001
msgid ""
"Please make sure the file system you want to save debug logs on is mounted "
"before you continue."
msgstr ""
"पुढे जाण्यापूर्वी ज्या फाइल प्रणालीवर दोषमार्जन सत्रनोंदी संचयित करायच्या आहेत, ती आरोहित "
"झालेली आहे याची खात्री करा."

#. Type: error
#. Description
#. :sl2:
#: ../save-logs.templates:4001
msgid "Cannot save logs"
msgstr "सत्रनोंदी संचयित करता येत नाहीत"

#. Type: error
#. Description
#. :sl2:
#: ../save-logs.templates:4001
msgid "The directory \"${DIR}\" does not exist."
msgstr "\"${DIR}\" निर्देशिका अस्तित्वात नाही."

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:5001
msgid "Web server started, but network not running"
msgstr "महाजाल परीसेवक सुरु केला, परंतू नेटवर्क चालू नाही"

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:5001
msgid ""
"A simple web server has been started on this computer to serve log files and "
"debug info. However, the network is not set up yet. The web server will be "
"left running, and will be accessible once the network is configured."
msgstr ""
"सत्रनोंद फायली व दोषमार्जन माहिती साठवण्यासाठी साधा महाजाल परिसेवक या संगणकावर सुरू "
"केला गेला आहे. परंतू नेटवर्कची संरचना अद्याप झालेली नाही. महाजाल परिसेवक चालू राहील परंतू "
"त्याची संरचना केल्यावरच तो प्रवेशायोग्य होईल."

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:6001
msgid "Web server started"
msgstr "महाजाल परिसेवक चालू झाला"

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:6001
msgid ""
"A simple web server has been started on this computer to serve log files and "
"debug info. An index of all the available log files can be found at http://"
"${ADDRESS}/"
msgstr ""
"सत्रनोंद फायली व दोषमार्जन माहिती साठवण्यासाठी साधा जाल परिसेवक या संगणकावर सुरू "
"झाला आहे. सर्व उपलब्ध सत्रनोंद फायलींची सुची http://${ADDRESS}/ वर सापडेल."

#. Type: note
#. Description
#. :sl2:
#: ../save-logs.templates:7001
msgid "Insert formatted floppy in drive"
msgstr "ड्राइव्ह मध्ये संरुपित फ्लॉपी टाका"

#. Type: note
#. Description
#. :sl2:
#: ../save-logs.templates:7001
msgid "Log files and debug info will be copied into this floppy."
msgstr "सत्रनोंद फायली व दोषमार्जन माहिती फ्लॉपीवर साठवली जात आहे."

#. Type: note
#. Description
#. :sl2:
#: ../save-logs.templates:7001
msgid ""
"The information will also be stored in /var/log/installer/ on the installed "
"system."
msgstr "ही माहिती अधिष्ठापित प्रणालीवर /var/log/installar/ येथेही साठवली जाईल."

#. Type: error
#. Description
#. :sl3:
#: ../save-logs.templates:8001
msgid "Failed to mount the floppy"
msgstr "फ्लॉपीचे आरोहण अयशस्वी"

#. Type: error
#. Description
#. :sl3:
#: ../save-logs.templates:8001
msgid ""
"Either the floppy device cannot be found, or a formatted floppy is not in "
"the drive."
msgstr "एकतर फलॉपी उपकरण सापडत नसेल किंवा संरुपित केलेली फ्लॉपी ड्राइव्ह मध्ये नसेल."

#. Type: text
#. Description
#. :sl1:
#. finish-install progress bar item
#: ../save-logs.templates:9001
msgid "Gathering information for installation report..."
msgstr "अधिष्ठापना अहवालासाठी माहिती गोळा केली जात आहे..."
